This module provides a links for exporting events to Outlook.

## Usage
1. First create a content type with date field.
2. Open the manage display in the content type.
3. In manage display for the date field choose the format as 
   -> Event to Outlook.
4. Edit the date field's format setting if you want location 
   and body.

## Settings
All settings are set in the field formatter 
settings in _Manage Display_ for the selected entity type.

* **Location Field**: 
	An optional field to use as the location in calendar events.
* **Description Field**: 
	An optional field to use as a description in calendar events.
* **Show for Past Events**: 
	Determines whether the widget will be displayed for past events.
